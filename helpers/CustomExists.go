package helpers

import (
	"gitlab.com/livesocket/command-runner/models"
	"gitlab.com/livesocket/service/v2"
)

// CustomExists Checks if a custom command exists for the channel with the same name
func CustomExists(service *service.Service, channel string, name string) (bool, error) {
	custom, err := models.GetCustom(service, name, channel)
	if err != nil {
		return false, err
	}
	return custom != nil, nil
}
