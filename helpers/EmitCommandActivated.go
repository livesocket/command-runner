package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/command-runner/models"
	"gitlab.com/livesocket/service/v2"
)

// EmitCommandActivated Emits "event.command.activated"
func EmitCommandActivated(service *service.Service, command *models.Custom) error {
	return service.Publish("event.command.activated", nil, wamp.List{command}, nil)
}
