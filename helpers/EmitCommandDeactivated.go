package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/command-runner/models"
	"gitlab.com/livesocket/service/v2"
)

// EmitCommandDeactivated Emits "event.command.deactivated"
func EmitCommandDeactivated(service *service.Service, command *models.Custom) error {
	return service.Publish("event.command.deactivated", nil, wamp.List{command}, nil)
}
