FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/command-runner
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/command-runner
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o command-runner

FROM scratch as release
COPY --from=builder /repos/command-runner/command-runner /command-runner
EXPOSE 8080
ENTRYPOINT ["/command-runner"]